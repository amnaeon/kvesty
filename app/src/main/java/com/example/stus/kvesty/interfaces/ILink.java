package com.example.stus.kvesty.interfaces;

import com.example.stus.kvesty.models.mongo_models.MongoGameModel;
import com.example.stus.kvesty.models.mongo_models.MongoHintModel;
import com.example.stus.kvesty.models.mongo_models.MongoTeamModel;
import com.example.stus.kvesty.models.recycler_view_models.HintModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ILink {

    @POST("getAllGames")
    Call<List<MongoGameModel>> getAllGame();

    @POST("getTeam")
    Call<MongoTeamModel> getTeamById(@Query("name") String name, @Query("pass") String pass, @Query("gameId") String gameId, @Query("userId") String userId);

    @GET("getTeamByGameId")
    Call<List<MongoTeamModel>> getAllTeams(@Query("gameId") String gameId);

    @GET("getHintByTeamId")
    Call<List<HintModel>> getAllHintByTeamID(@Query("teamId") String teamId);

    @POST("createNewHint")
    Call<Object> createHint(@Query("teamId") String teamId, @Query("type") int type, @Query("text") String text);

    String URL = "https://tranquil-tor-71992.herokuapp.com/";
    Gson gson = new GsonBuilder()
            .setLenient()
            .create();
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();


}

package com.example.stus.kvesty.models.mongo_models;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MongoGameModel {
    private String gameId = "";
    private String name = "";
    private String description = "";
    private String backgroundUrl = "";
    private String photoUrl = "";
    private int countOfTask = 0;
    private List<MongoTaskModel> task = new ArrayList<>();

    public MongoGameModel(String json) {
        try {
            JSONObject object = new JSONObject(json);
            gameId = object.getString("gameId");
            name = object.getString("name");

            description = object.getString("description");
            backgroundUrl = object.getString("backgroundUrl");
            photoUrl = object.getString("photoUrl");
            countOfTask = object.getInt("countOfTask");
            JSONArray tasks = object.getJSONArray("task");
            for (int i = 0; i < tasks.length(); i++) {
                task.add(new MongoTaskModel(tasks.getJSONObject(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public String getGameId() {
        return gameId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getBackgroundUrl() {
        return backgroundUrl;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public int getCountOfTask() {
        return countOfTask;
    }

    public List<MongoTaskModel> getTaskModelList() {
        return task;
    }
}

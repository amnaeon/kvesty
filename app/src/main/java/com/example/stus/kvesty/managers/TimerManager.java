package com.example.stus.kvesty.managers;

/**
 * Created by stus on 17.10.17.
 */

public class TimerManager {
    private static final TimerManager ourInstance = new TimerManager();

    public static TimerManager getInstance() {
        return ourInstance;
    }

    private IOnTick onTick;
    private IOnTimerFinish onTimerFinish;

    private TimerManager() {
    }

    public void setOnTick(IOnTick onTick) {
        this.onTick = onTick;
    }

    public IOnTick getOnTick() {
        return onTick;
    }

    public void setOnTimerFinish(IOnTimerFinish onTimerFinish) {
        this.onTimerFinish = onTimerFinish;
    }

    public IOnTimerFinish getOnTimerFinish() {
        return onTimerFinish;
    }

    public interface IOnTick {
        void onTick(String time);
    }

    public interface IOnTimerFinish {
        void onFinsh();
    }
}

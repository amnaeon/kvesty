package com.example.stus.kvesty.models.mongo_models;

/**
 * Created by stus on 05.10.17.
 */

public class MongoTeamModel {
    private String id = "";
    private String gameId = "";
    private String name = "";
    private String teamPassword = "";
    private int progress = 0;
    private int hintProgress = 0;

    public MongoTeamModel(String id, String gameId, String name, String teamPass, int teamProgress, int hintProgress) {
        this.id = id;
        this.gameId = gameId;
        this.name = name;
        this.teamPassword = teamPass;
        this.progress = teamProgress;
        this.hintProgress = hintProgress;
    }

    public String getId() {
        return id;
    }

    public String getGameId() {
        return gameId;
    }

    public String getName() {
        return name;
    }

    public String getTeamPass() {
        return teamPassword;
    }

    public int getTeamProgress() {
        return progress;
    }

    public int getHintProgress() {
        return hintProgress;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTeamPass(String teamPassword) {
        this.teamPassword = teamPassword;
    }

    public void setTeamProgress(int progress) {
        this.progress = progress;
    }

    public void setHintProgress(int hintProgress) {
        this.hintProgress = hintProgress;
    }
}

package com.example.stus.kvesty;

import com.example.stus.kvesty.interfaces.ISocketResponce;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;

import tech.gusavila92.websocketclient.WebSocketClient;

import static com.example.stus.kvesty.Const.SocketCmd.GAME_END;
import static com.example.stus.kvesty.Const.SocketCmd.GAME_START;
import static com.example.stus.kvesty.Const.SocketCmd.GOT_A_HINT;
import static com.example.stus.kvesty.Const.SocketCmd.TASK_COMPLETE;
import static com.example.stus.kvesty.Const.SocketCmd.TIMER_START;
import static com.example.stus.kvesty.Const.SocketCmd.TIMER_STOP;

/**
 * Created by stus on 13.08.17.
 */

public class Socket {
    private WebSocketClient webSocketClient;
    private ISocketResponce socketResponce;

    public void createWebSocketClient() {
        URI uri;
        try {
            uri = new URI("ws://tranquil-tor-71992.herokuapp.com/connection");
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }

        webSocketClient = new WebSocketClient(uri) {
            @Override
            public void onOpen() {
                System.out.println("onOpen");
//                webSocketClient.send("");

            }

            @Override
            public void onTextReceived(String message) {
                try {
                    JSONObject JSONMessage = new JSONObject(message);
                    int command = JSONMessage.getInt("cmd");
                    switch (command) {
                        case TASK_COMPLETE:
                            socketResponce.complete();
                            break;
                        case GOT_A_HINT:
                            socketResponce.getHint(JSONMessage.getString("teamId"));
                            break;
                        case TIMER_START:
                            break;
                        case TIMER_STOP:

                            break;
                        case GAME_START:
                            socketResponce.gameStart();
                            break;
                        case GAME_END:
                            socketResponce.gameEnd(JSONMessage.getString("name"));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
                System.out.println("onTextReceived");
            }

            @Override
            public void onBinaryReceived(byte[] data) {
                System.out.println("onBinaryReceived");
            }

            @Override
            public void onPingReceived(byte[] data) {
                System.out.println("onPingReceived");
            }

            @Override
            public void onPongReceived(byte[] data) {
                System.out.println("onPongReceived");
            }

            @Override
            public void onException(Exception e) {
                System.out.println(e.getMessage());
            }

            @Override
            public void onCloseReceived() {
                System.out.println("onCloseReceived");
            }
        };

        webSocketClient.connect();

    }

    public void sendComand(String command) {
        webSocketClient.send(command);
    }


    public void close() {
        webSocketClient.close();
    }

    public void setSocketResponce(ISocketResponce socketResponce) {
        this.socketResponce = socketResponce;
    }
}
package com.example.stus.kvesty.interfaces;

import com.example.stus.kvesty.models.mongo_models.MongoTeamModel;

/**
 * Created by stus on 11.10.17.
 */

public interface IOnTeamClick {
    void onClick(MongoTeamModel mongoTeamModel);
}

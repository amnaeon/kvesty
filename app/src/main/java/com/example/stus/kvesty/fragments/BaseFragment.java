package com.example.stus.kvesty.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.stus.kvesty.MainActivity;
import com.example.stus.kvesty.R;
import com.example.stus.kvesty.lambda_util.IUiItemStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 12.02.16.
 */
public abstract class BaseFragment extends Fragment implements IUiItemStatus {
    protected boolean isAvalible;
    protected View parent;

    public MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }

    @Override
    public void onResume() {
        this.isAvalible = true;
        super.onResume();
    }

    @Override
    public void onPause() {
        isAvalible = false;
        super.onPause();
    }

    @Override
    public boolean isGetResponse() {
        return isAvalible;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.parent = inflater.inflate(viewID(), null, false);
        setHasOptionsMenu(true);
        getInit().exec();
        return parent;
    }


    protected String getTitle() {
        return getMainActivity().getString(R.string.app_name);
    }

    protected boolean enableDriwer() {
        return true;
    }


    protected abstract int viewID();

    protected Initer getInit() {
        return new Initer();
    }

    public boolean onBackPressed() {
        return true;
    }

    protected final class Initer {
        List<Runnable> initList;

        public Initer() {
            initList = new ArrayList<>();
        }

        public Initer add(Runnable runnable) {
            initList.add(runnable);
            return this;
        }

        private void exec() {
            for (Runnable r : initList) {
                r.run();
            }
        }

    }
}
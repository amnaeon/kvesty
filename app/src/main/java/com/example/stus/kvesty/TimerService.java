package com.example.stus.kvesty;


import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;

import com.example.stus.kvesty.managers.TimerManager;

import java.util.concurrent.TimeUnit;

import static com.example.stus.kvesty.Const.BundleKeys.DURATION;

public class TimerService extends Service {
    private CountDownTimer counTimer;
    private boolean isTimerFinish = false;

    public TimerService() {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (counTimer == null || isTimerFinish) {
            isTimerFinish = false;
            counTimer = new CountDownTimer(intent.getLongExtra(DURATION, 0), 1000) {

                public void onTick(long millis) {
                    String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                            TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                            TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                    if (TimerManager.getInstance().getOnTick() != null) {
                        TimerManager.getInstance().getOnTick().onTick(hms);
                    }
                }

                public void onFinish() {
                    isTimerFinish = true;
                    if (TimerManager.getInstance().getOnTimerFinish() != null) {
                        TimerManager.getInstance().getOnTimerFinish().onFinsh();
                    }

                }
            }.start();
        }
        return super.onStartCommand(intent, flags, startId);
    }
}

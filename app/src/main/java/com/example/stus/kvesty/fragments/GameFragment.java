package com.example.stus.kvesty.fragments;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.stus.kvesty.App;
import com.example.stus.kvesty.MainActivity;
import com.example.stus.kvesty.Socket;
import com.example.stus.kvesty.TimerService;
import com.example.stus.kvesty.interfaces.ILink;
import com.example.stus.kvesty.interfaces.IOnAnswerSend;
import com.example.stus.kvesty.QrCodeActivity;
import com.example.stus.kvesty.R;
import com.example.stus.kvesty.Utils;
import com.example.stus.kvesty.adapders.ProgressAdapter;
import com.example.stus.kvesty.interfaces.ISocketResponce;
import com.example.stus.kvesty.managers.TimerManager;
import com.example.stus.kvesty.managers.UiManager;
import com.example.stus.kvesty.models.mongo_models.MongoGameModel;
import com.example.stus.kvesty.models.mongo_models.MongoTeamModel;
import com.example.stus.kvesty.models.recycler_view_models.ProgressDataModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.GRAY;
import static com.example.stus.kvesty.Const.BundleKeys.DIALOG_TEXT;
import static com.example.stus.kvesty.Const.BundleKeys.DURATION;
import static com.example.stus.kvesty.Const.BundleKeys.GAME_ID;
import static com.example.stus.kvesty.Const.BundleKeys.TEAM_ID;
import static com.example.stus.kvesty.Const.HintTypes.IMAGE_TYPE;
import static com.example.stus.kvesty.Const.HintTypes.VIDEO_TYPE;
import static com.example.stus.kvesty.Const.SharedPreference.HINT_PROGRESS;
import static com.example.stus.kvesty.Const.SocketCmd.GAME_END;
import static com.example.stus.kvesty.Const.SocketCmd.TASK_COMPLETE;

public class GameFragment extends BaseFragment implements IOnAnswerSend, ISocketResponce {
    private static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
    private static final int NOTIFY_ID = 101;
    private String gameId = "";
    private String teamId = "";
    private boolean isTimerFinish = false;
    private boolean isGameStart = false;
    private SharedPreferences sharedPreferences;
    private RecyclerView progressList;
    private ImageView taskPicture;
    private ImageView fullScreenTaskPicture;
    private ImageView camera;
    private TextView timer;
    private Button hint;
    private Button sendAnswer;
    private MongoGameModel gameModel;
    private List<MongoTeamModel> teamModels = new ArrayList<>();
    private MongoTeamModel currentTeam;
    private Dialog fd;
    private CountDownTimer countTimer;
    private Socket socket;
    private VideoView videoView;

    @Override
    protected int viewID() {
        return R.layout.game_fragment;
    }

    public static BaseFragment getInstance(String teamId, String gameId) {
        GameFragment fragment = new GameFragment();
        Bundle arg = new Bundle();
        arg.putString(TEAM_ID, teamId);
        arg.putString(GAME_ID, gameId);
        fragment.setArguments(arg);
        return fragment;

    }

    @Override
    protected Initer getInit() {
        return super.getInit()
                .add(this::readArg)
                .add(this::getTeams)
                .add(this::readAssets)
                .add(this::init)
                .add(this::initQrReader)
                .add(this::sendAnswer)
                .add(() -> parent.findViewById(R.id.timer_ico).setOnClickListener(v -> UiManager.showGameTimerFragment()))
                ;
    }

    private void readArg() {
        teamId = getArguments().getString(TEAM_ID);
        gameId = getArguments().getString(GAME_ID);
    }

    private void getTeams() {
        ILink iLink1 = ILink.retrofit.create(ILink.class);
        Call<List<MongoTeamModel>> call1 = iLink1.getAllTeams(gameId);
        call1.enqueue(new Callback<List<MongoTeamModel>>() {
            @Override
            public void onResponse(Call<List<MongoTeamModel>> call1, Response<List<MongoTeamModel>> response) {
                teamModels.clear();
                teamModels.addAll(response.body());
                for (int i = 0; i < teamModels.size(); i++) {
                    if (teamModels.get(i).getId().equals(teamId)) {
                        currentTeam = teamModels.get(i);
                        updateTask();
                        TimerManager.getInstance().setOnTimerFinish(GameFragment.this::changeTask);
                        break;
                    }
                }
            }


            @Override
            public void onFailure(Call<List<MongoTeamModel>> call, Throwable t) {

            }
        });

    }

    private void readAssets() {
        gameModel = new MongoGameModel(Utils.readTextFromAssets("game.json"));

    }

    private void init() {
        progressList = parent.findViewById(R.id.progress_list);
        timer = parent.findViewById(R.id.timer);
        taskPicture = parent.findViewById(R.id.task_picture);
        hint = parent.findViewById(R.id.hint);
        camera = parent.findViewById(R.id.camera);
        sendAnswer = parent.findViewById(R.id.send_answer);
        videoView = parent.findViewById(R.id.task_video);
        gameModel = new MongoGameModel(Utils.readTextFromAssets("game.json"));
        fullScreenTaskPicture = parent.findViewById(R.id.task_picture_full_screen);
        socket = new Socket();
        socket.createWebSocketClient();
        socket.setSocketResponce(this);

        sharedPreferences = App.getCurrentActivity().getPreferences(Context.MODE_PRIVATE);

        hint.setOnClickListener(v -> UiManager.showHintFragment(currentTeam.getId()));
    }

    private void initQrReader() {
        camera.setOnClickListener(v -> {
            Intent intent = new Intent(App.getCurrentActivity(), QrCodeActivity.class);
            startActivity(intent);
        });
    }


    private void sendAnswer() {
        sendAnswer.setOnClickListener(v -> {
            fd = new Dialog();
            Bundle arg = new Bundle();
            arg.putString(DIALOG_TEXT, "Введите ответ");
            fd.setArguments(arg);
            fd.setOnAnswerSend(this);
            fd.show(getFragmentManager(), "ds");
        });
    }

    private void updateTask() {
        int hintProgress = 0;
        if (sharedPreferences != null) {
            hintProgress = sharedPreferences.getInt(HINT_PROGRESS, 0);
        }
        if (isGameStart) {
            String videoUrl = gameModel.getTaskModelList().get(currentTeam.getTeamProgress()).getHintModelList().get(hintProgress).getVideoUrl();
            ILink iLink = ILink.retrofit.create(ILink.class);
            if (!videoUrl.equals("null")) {
                Call<Object> call = iLink.createHint(currentTeam.getId(), VIDEO_TYPE,videoUrl);
                call.enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        initVideo();

                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {

                    }
                });
            } else {
                String photoUrl = gameModel.getTaskModelList().get(currentTeam.getTeamProgress()).getHintModelList().get(hintProgress).getPhotoUrl();
                Call<Object> call = iLink.createHint(currentTeam.getId(), IMAGE_TYPE, photoUrl);
                int finalHintProgress = hintProgress;
                call.enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        videoView.pause();
                        videoView.setVisibility(View.GONE);
                        taskPicture.setVisibility(View.VISIBLE);
                        taskPicture.setBackgroundDrawable(Utils.getDravableFromAssets(photoUrl));
                        taskPicture.setOnClickListener(v -> {
                            fullScreenTaskPicture.setBackgroundDrawable(Utils.getDravableFromAssets(photoUrl));
                            fullScreenTaskPicture.setVisibility(View.VISIBLE);
                            fullScreenTaskPicture.setOnClickListener(view -> fullScreenTaskPicture.setVisibility(View.GONE));
                        });
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {

                    }
                });
            }
            Intent intent = new Intent(App.getCurrentActivity(), TimerService.class);
            intent.putExtra(DURATION, gameModel.getTaskModelList().get(currentTeam.getTeamProgress()).getTime());
            App.getCurrentActivity().startService(new Intent(intent));
            initProgressList();
            initTimer();
        }

    }

    private void initVideo() {

        taskPicture.setBackgroundDrawable(App.getCurrentActivity().getResources().getDrawable(R.drawable.play_ico));
        taskPicture.setOnClickListener(v -> {

            videoView.setVisibility(View.VISIBLE);
            String path = "android.resource://" + App.getCurrentActivity().getPackageName() + "/" + R.raw.w;
            videoView.setVideoURI(Uri.parse(path));
            videoView.setBackgroundColor(Color.TRANSPARENT);
            videoView.setZOrderOnTop(true);
//            videoView.setRotation(180f);
            videoView.start();

            videoView.setOnTouchListener((view, motionEvent) -> {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (videoView.isPlaying()) {
                        videoView.pause();
                    } else {
                        videoView.start();
                    }
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                    videoView.setVisibility(View.GONE);
                }

                return true;
            });

        });
    }

    private void initProgressList() {
        List<ProgressDataModel> progressListData = new ArrayList<>();
        for (int i = 0; i < teamModels.size(); i++) {
            progressListData.add(new ProgressDataModel(teamModels.get(i).getName(), String.valueOf(teamModels.get(i).getTeamProgress()), String.valueOf(gameModel.getTaskModelList().size())));
        }
        ProgressAdapter adapter = new ProgressAdapter(progressListData);
        progressList.setLayoutManager(new LinearLayoutManager(App.getCurrentActivity(), LinearLayoutManager.HORIZONTAL, false));
        progressList.setAdapter(adapter);
    }


    private void initTimer() {

        if (countTimer == null || isTimerFinish) {
            isTimerFinish = false;
            countTimer = new CountDownTimer(gameModel.getTaskModelList().get(currentTeam.getTeamProgress()).getHintModelList().get(sharedPreferences.getInt(HINT_PROGRESS, 0)).getTime(), 1000) {

                public void onTick(long millis) {
                    String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                            TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                            TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                    timer.setText(hms);
                }

                public void onFinish() {
                    isTimerFinish = true;
                    changeHint();
                }
            }.start();
        }

    }

    private void changeHint() {
        try {
            if (sharedPreferences.getInt(HINT_PROGRESS, 0) != gameModel.getTaskModelList().get(currentTeam.getTeamProgress()).getHintModelList().size() - 1) {
                currentTeam.setHintProgress(currentTeam.getHintProgress() + 1);
                SharedPreferences.Editor ed = sharedPreferences.edit();

                ed.putInt(HINT_PROGRESS, currentTeam.getHintProgress());
                ed.commit();
                countTimer.cancel();
                updateTask();
//                initTimer();
            } else {
                timer.setText("00:00:00");
            }
        } catch (Exception e) {
            timer.setText("00:00:00");

        }
    }



    @Override
    public void send(String text) {
        fd.dismiss();
        if (Utils.taminotoC(text.trim(), gameModel.getTaskModelList().get(currentTeam.getTeamProgress()).getAnswer()) * 100 >= gameModel.getTaskModelList().get(currentTeam.getTeamProgress()).getBorders()) {

            currentTeam.setHintProgress(0);
            SharedPreferences.Editor ed = sharedPreferences.edit();
            ed.putInt(HINT_PROGRESS, 0);
            ed.commit();
            currentTeam.setTeamProgress(currentTeam.getTeamProgress() + 1);
            updateSocket();
            initProgressList();
            if (gameModel.getCountOfTask() != currentTeam.getTeamProgress()) {
                countTimer.cancel();
                countTimer = null;
                initTimer();
            } else {
                Toast.makeText(App.getCurrentActivity(), "Вы выграли", Toast.LENGTH_LONG).show();
                JSONObject object = new JSONObject();
                try {
                    object.put("cmd", GAME_END);
                    object.put("teamId", currentTeam.getId());
                    socket.sendComand(object.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                finishGame();


            }
        } else {
            sendAnswer.setClickable(false);
            sendAnswer.setTextColor(GRAY);
            new CountDownTimer(5000, 1000) {

                @Override
                public void onTick(long l) {

                }

                @Override
                public void onFinish() {
                    sendAnswer.setClickable(true);
                    sendAnswer.setTextColor(BLACK);

                }
            }.start();
            Toast.makeText(App.getCurrentActivity(), "неверно", Toast.LENGTH_LONG).show();
        }
    }

    private void finishGame() {
        isGameStart = false;
        countTimer.cancel();
        timer.setText("00:00:00");
        camera.setClickable(false);
        hint.setClickable(false);
        sendAnswer.setClickable(false);
        parent.findViewById(R.id.timer_ico).setClickable(false);
        App.getCurrentActivity().stopService(new Intent(App.getCurrentActivity(), TimerService.class));
    }

    private void updateSocket() {
        try {
            JSONObject comand = new JSONObject();
            comand.put("cmd", TASK_COMPLETE);
            comand.put("teamId", currentTeam.getId());
            socket.sendComand(comand.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void complete() {
        getTeams();
        resetHint();

    }

    private void resetHint() {
        SharedPreferences.Editor ed = sharedPreferences.edit();
        ed.putInt(HINT_PROGRESS, 0);
        ed.commit();
    }

    @Override
    public void gameStart() {
        App.getCurrentActivity().runOnUiThread(() -> {
            isGameStart = true;
            initTimer();
            updateTask();
        });

    }

    @Override
    public void gameEnd(String name) {
        if (!currentTeam.getName().equals(name)) {
            App.getCurrentActivity().runOnUiThread(() -> {
                Toast.makeText(App.getCurrentActivity(), "команда " + name + " победила", Toast.LENGTH_LONG).show();
            });
        }
       resetHint();
    }

    @Override
    public void getHint(String teamId) {
        if (teamId.equals(currentTeam.getId())) {
            Context context = App.getCurrentActivity();
            Intent notificationIntent = new Intent(context, MainActivity.class);
            PendingIntent contentIntent = PendingIntent.getActivity(context,
                    0, notificationIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT);

            Resources res = context.getResources();
            Notification.Builder builder = new Notification.Builder(context);

            builder.setContentIntent(contentIntent)
                    .setSmallIcon(R.drawable.camera_ico)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.camera_ico))
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setContentTitle(gameModel.getName())
                    .setContentText("У вас новая подсказка");
            Notification notification = builder.getNotification();
            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(NOTIFY_ID, notification);
        }
    }

    private void changeTask() {
        Toast.makeText(App.getCurrentActivity(), "Вы не успели выполнить задание", Toast.LENGTH_LONG).show();

        if (currentTeam.getTeamProgress() != gameModel.getTaskModelList().size()) {
            updateSocket();
        } else Toast.makeText(App.getCurrentActivity(), "Игра окончена", Toast.LENGTH_LONG).show();
    }
}

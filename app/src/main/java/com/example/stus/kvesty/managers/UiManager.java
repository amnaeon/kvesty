package com.example.stus.kvesty.managers;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.example.stus.kvesty.MainActivity;
import com.example.stus.kvesty.R;
import com.example.stus.kvesty.fragments.BaseFragment;
import com.example.stus.kvesty.fragments.GameFragment;
import com.example.stus.kvesty.fragments.GameTimerFragment;
import com.example.stus.kvesty.fragments.HintFragment;
import com.example.stus.kvesty.fragments.OperatorFragment;
import com.example.stus.kvesty.fragments.SplashFragment;
import com.example.stus.kvesty.fragments.StartFragment;

import java.util.Stack;


/**
 * Created on 12.02.16.
 */
public class UiManager {
    private static UiManager ourInstance = new UiManager();
    private MainActivity parent;

    private Stack<BaseFragment> backStack = new Stack<>();

    public static UiManager getInstance() {
        return ourInstance;
    }

    private UiManager() {
    }

    public void init(MainActivity parent) {
        this.parent = parent;
        parent.setContentView(R.layout.activity_main);


    }

    public boolean back() {
        if (backStack.size() < 2) return true;
        if (backStack.lastElement().onBackPressed()) {
            backStack.pop();
            showFragment(backStack.pop());
            return false;
        }
        return true;
    }


    protected void showFragment(BaseFragment fragment) {
        FragmentManager manager = parent.getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();

        backStack.push(fragment);

        ft = manager.beginTransaction();
        ft.replace(R.id.main_container, fragment);
        if (!commitTransaction(ft)) return;
        if (manager == null) return;
        manager.executePendingTransactions();

    }

    private boolean commitTransaction(FragmentTransaction ft) {
        try {
            ft.commit();
        } catch (Exception e) {
            //todo log it
            try {
                ft.commitAllowingStateLoss();
            } catch (Exception e1) {
                //todo Log it
                return false;
            }
        }
        return true;
    }

    public static void showStartFragment() {
        getInstance().showFragment(StartFragment.getInstance());
    }

    public static void showGameFragment(String teamId) {
        getInstance().showFragment(GameFragment.getInstance(teamId));
    }

    public static void showHintFragment(String teamId) {
        getInstance().showFragment(HintFragment.getInstance(teamId));
    }

    public static void showSplashFragment() {
        getInstance().showFragment(SplashFragment.getInstance());
    }

    public static void showOperatorFragment(String gameId) {
        getInstance().showFragment(OperatorFragment.getInstance(gameId));
    }

    public static void showGameTimerFragment(){
        getInstance().showFragment(GameTimerFragment.getInstance());
    }
}

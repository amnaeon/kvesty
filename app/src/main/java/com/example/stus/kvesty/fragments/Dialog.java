package com.example.stus.kvesty.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.stus.kvesty.interfaces.IOnAnswerSend;
import com.example.stus.kvesty.R;

import static com.example.stus.kvesty.Const.BundleKeys.DIALOG_TEXT;

/**
 * Created by stus on 06.10.17.
 */

public class Dialog extends DialogFragment {
    private IOnAnswerSend onAnswerSend;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dialog, container, false);
        TextView tv = v.findViewById(R.id.answer);
        Button ok = v.findViewById(R.id.ok);
        tv.setHint(getArguments().getString(DIALOG_TEXT));
        ok.setOnClickListener(v1 -> {
            if (tv.getText().length() > 0) {
                onAnswerSend.send(String.valueOf(tv.getText()));
            }
        });
        return v;
    }

    public void setOnAnswerSend(IOnAnswerSend onAnswerSend) {
        this.onAnswerSend = onAnswerSend;
    }
}

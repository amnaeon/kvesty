package com.example.stus.kvesty.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.stus.kvesty.App;
import com.example.stus.kvesty.R;
import com.example.stus.kvesty.adapders.HintAdapter;
import com.example.stus.kvesty.interfaces.ILink;
import com.example.stus.kvesty.models.recycler_view_models.HintModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.stus.kvesty.Const.BundleKeys.TEAM_ID;

/**
 * Created by stus on 06.10.17.
 */

public class HintFragment extends BaseFragment {
    private String teamId = "";

    @Override
    protected int viewID() {
        return R.layout.hint_fragment;
    }

    public static BaseFragment getInstance(String teamId) {
        Bundle arg = new Bundle();
        arg.putString(TEAM_ID, teamId);
        HintFragment hintFragment = new HintFragment();
        hintFragment.setArguments(arg);
        return hintFragment;
    }

    @Override
    protected Initer getInit() {
        return super.getInit().add(this::readArg).add(this::initList);
    }

    private void readArg() {
        teamId = getArguments().getString(TEAM_ID);
    }

    private void initList() {
        ILink iLink = ILink.retrofit.create(ILink.class);
        Call<List<HintModel>> call = iLink.getAllHintByTeamID(teamId);
        call.enqueue(new Callback<List<HintModel>>() {
            @Override
            public void onResponse(Call<List<HintModel>> call, Response<List<HintModel>> response) {
                RecyclerView hintList = parent.findViewById(R.id.hint_list);
                HintAdapter hintAdapter = new HintAdapter(response.body());
                hintList.setLayoutManager(new LinearLayoutManager(App.getCurrentActivity()));
                hintList.setAdapter(hintAdapter);
            }

            @Override
            public void onFailure(Call<List<HintModel>> call, Throwable t) {

            }
        });

    }
}

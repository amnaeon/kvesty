package com.example.stus.kvesty;

import android.app.Application;
import android.support.annotation.Nullable;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class App extends Application {
    public static Realm realm = null;
    private static MainActivity mainActivity;


    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static void registerActivity(MainActivity mainActivity) {
        App.mainActivity = mainActivity;
    }

    @Nullable
    public static MainActivity getCurrentActivity() {
        return mainActivity;
    }

    public static Realm rm() {
        if (realm == null) {
            RealmConfiguration realmConfig = new RealmConfiguration.Builder(mainActivity).build();
            Realm.setDefaultConfiguration(realmConfig);
            realm = Realm.getInstance(realmConfig);
        }
        return realm;
    }
}

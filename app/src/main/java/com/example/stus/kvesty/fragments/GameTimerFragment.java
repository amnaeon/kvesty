package com.example.stus.kvesty.fragments;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.stus.kvesty.R;
import com.example.stus.kvesty.managers.TimerManager;

import java.util.concurrent.TimeUnit;

import static com.example.stus.kvesty.Const.BundleKeys.DURATION;

/**
 * Created by stus on 16.10.17.
 */

public class GameTimerFragment extends BaseFragment {

    @Override
    protected int viewID() {
        return R.layout.game_timer_fragment;
    }

    public static GameTimerFragment getInstance(){
        return new GameTimerFragment();
    }


    @Override
    protected Initer getInit() {
        return super.getInit()
                .add(this::initTimer);
    }

    private void initTimer() {
        TimerManager.getInstance().setOnTick(text -> ((TextView)parent.findViewById(R.id.timer)).setText(text));
    }
}

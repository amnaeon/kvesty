package com.example.stus.kvesty.models.recycler_view_models;

/**
 * Created by stus on 05.10.17.
 */

public class ProgressDataModel {
    private String name = "";
    private String progress = "";
    private String taskCount = "";

    public ProgressDataModel(String name, String progress, String taskCount) {
        String[] namesWords = name.split(" ");
        for (String namesWord : namesWords) {
            this.name += Character.toUpperCase(namesWord.charAt(0)) + ".";
        }
        this.progress = progress;
        this.taskCount = taskCount;
    }

    public String getName() {
        return name;
    }

    public String getProgress() {
        return progress;
    }

    public String getTaskCount() {
        return taskCount;
    }
}

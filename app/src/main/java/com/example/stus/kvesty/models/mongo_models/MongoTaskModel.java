package com.example.stus.kvesty.models.mongo_models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stus on 06.10.17.
 */

public class MongoTaskModel {
    private String name = "";
    private String answer = "";
    private int borders = 0;
    private long time = 0;
    private List<MongoHintModel> hint = new ArrayList<>();

    public long getTime() {
        return time;
    }

    public MongoTaskModel(JSONObject jsonObject) throws JSONException {
        name = jsonObject.getString("name");
        answer = jsonObject.getString("answer");
        borders = jsonObject.getInt("borders");
        time = jsonObject.getLong("time");

        JSONArray hints = jsonObject.getJSONArray("hint");
        for (int i = 0; i < hints.length(); i++) {
            hint.add(new MongoHintModel(hints.getJSONObject(i)));
        }
    }

    public String getName() {
        return name;
    }

    public String getAnswer() {
        return answer;
    }

    public int getBorders() {
        return borders;
    }

    public List<MongoHintModel> getHintModelList() {
        return hint;
    }
}

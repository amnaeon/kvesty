package com.example.stus.kvesty.models.recycler_view_models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by stus on 06.10.17.
 */

public class HintModel {
    private String teamId = "";
    private int type = 0;
    private String text = "";

    public HintModel(String teamId, int type, String text) {
        this.teamId = teamId;
        this.type = type;
        this.text = text;
    }

    public HintModel(String json) {
        try {
            JSONObject object = new JSONObject(json);
            teamId = object.getString("teamId");
            type = object.getInt("type");
            text = object.getString("text");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getTeamId() {
        return teamId;
    }

    public int getType() {
        return type;
    }

    public String getText() {
        return text;
    }
}

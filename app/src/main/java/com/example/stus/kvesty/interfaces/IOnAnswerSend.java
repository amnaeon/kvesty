package com.example.stus.kvesty.interfaces;

/**
 * Created by stus on 06.10.17.
 */

public interface IOnAnswerSend {
    void send(String text);
}

package com.example.stus.kvesty.fragments;

import android.os.Handler;
import android.os.Looper;

import com.example.stus.kvesty.Const;
import com.example.stus.kvesty.R;
import com.example.stus.kvesty.managers.UiManager;

public class SplashFragment extends BaseFragment {
    @Override
    protected int viewID() {
        return R.layout.splash;
    }

    public static BaseFragment getInstance() {
        return new SplashFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        new Handler(Looper.getMainLooper()).postDelayed(UiManager::showStartFragment, Const.SPLASH_DELLAY);

    }
}

package com.example.stus.kvesty.models;

import java.util.Random;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

import static com.example.stus.kvesty.App.rm;

/**
 * Created by stus on 06.10.17.
 */

public class StateModel extends RealmObject {
    @PrimaryKey
    private int id = new Random().nextInt(10303);
    private int currentTask = 0;
    private int currentHint = 0;
    private String userId = "";

    public int getCurrentTask() {
        return currentTask;
    }

    public void setCurrentTask(int currentTask) {
        rm().beginTransaction();
        this.currentTask = currentTask;
        rm().commitTransaction();
    }

    public int getCurrentHint() {
        return currentHint;
    }

    public void setCurrentHint(int currentHint) {
        rm().beginTransaction();
        this.currentHint = currentHint;
        rm().commitTransaction();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        rm().beginTransaction();
        this.userId = userId;
        rm().commitTransaction();
    }
}

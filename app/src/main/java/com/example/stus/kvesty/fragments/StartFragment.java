package com.example.stus.kvesty.fragments;

import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.stus.kvesty.App;
import com.example.stus.kvesty.interfaces.ILink;
import com.example.stus.kvesty.interfaces.IOnClick;
import com.example.stus.kvesty.R;
import com.example.stus.kvesty.Utils;
import com.example.stus.kvesty.managers.UiManager;
import com.example.stus.kvesty.adapders.GameAdapter;
import com.example.stus.kvesty.models.mongo_models.MongoGameModel;
import com.example.stus.kvesty.models.mongo_models.MongoTeamModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class StartFragment extends BaseFragment implements IOnClick {
    private TextView selectGame;
    private RecyclerView gameList;
    private RelativeLayout container;
    private ImageView gameImage;
    private Button downlawnd;
    private EditText comandName;
    private EditText password;
    private String gameId = "";

    @Override
    protected int viewID() {
        return R.layout.start_fragment;
    }

    public static BaseFragment getInstance() {
        return new StartFragment();
    }

    @Override
    protected Initer getInit() {
        return super.getInit().add(this::init)
                .add(this::initGameList)
                .add(this::initSelectClick)
                .add(this::initDownlawnd);
    }

    private void init() {
        selectGame = parent.findViewById(R.id.select_game);
        gameList = parent.findViewById(R.id.game_list);
        container = parent.findViewById(R.id.container);
        gameImage = parent.findViewById(R.id.game_immage);
        downlawnd = parent.findViewById(R.id.downlawnd);
        comandName = parent.findViewById(R.id.command_name_text);
        password = parent.findViewById(R.id.game_password_text);
    }

    private void initGameList() {
        ILink iLink = ILink.retrofit.create(ILink.class);
        Call<List<MongoGameModel>> call = iLink.getAllGame();
        call.enqueue(new Callback<List<MongoGameModel>>() {
            @Override
            public void onResponse(Call<List<MongoGameModel>> call, Response<List<MongoGameModel>> response) {

                GameAdapter adapter = new GameAdapter(response.body());
                adapter.setOnClick(StartFragment.this);
                gameList.setLayoutManager(new LinearLayoutManager(App.getCurrentActivity()));
                gameList.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<MongoGameModel>> call, Throwable t) {

            }
        });

    }

    private void initSelectClick() {
        selectGame.setOnClickListener(v -> {

            gameList.setVisibility(View.VISIBLE);
        });
    }

    private void initDownlawnd() {
        downlawnd.setOnClickListener(v -> {
            ILink iLink = ILink.retrofit.create(ILink.class);
            Call<MongoTeamModel> call = iLink.getTeamById(String.valueOf(comandName.getText()), String.valueOf(password.getText()), gameId, Utils.getId());
            call.enqueue(new Callback<MongoTeamModel>() {
                @Override
                public void onResponse(Call<MongoTeamModel> call, Response<MongoTeamModel> response) {
                    if (response.body().getGameId() != null) {
                        String teamId = response.body().getId();

                        UiManager.showGameFragment(teamId);
                    } else {
                        Toast.makeText(App.getCurrentActivity(), "Данные введены не верно", Toast.LENGTH_LONG).show();
                        comandName.setText("");
                        password.setText("");
                    }
                    Log.i("dsd", "dsdsdsd");
                }

                @Override
                public void onFailure(Call<MongoTeamModel> call, Throwable t) {

                }
            });


        });
    }

    @Override
    public void onClick(MongoGameModel model) {
        gameId = model.getGameId();
//        UiManager.showOperatorFragment(model.getGameId());
        byte[] background = Base64.decode(model.getPhotoUrl(), Base64.DEFAULT);
        container.setVisibility(View.VISIBLE);
        gameImage.setBackgroundDrawable(new BitmapDrawable(App.getCurrentActivity().getResources(), BitmapFactory.decodeByteArray(background, 0, background.length)));
    }
}

package com.example.stus.kvesty.holders;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.stus.kvesty.App;
import com.example.stus.kvesty.R;
import com.example.stus.kvesty.models.recycler_view_models.HintModel;

/**
 * Created by stus on 06.10.17.
 */

public class HintOperatorHolder extends RecyclerView.ViewHolder {
    private View itemView;

    public HintOperatorHolder(View itemView) {
        super(itemView);
        this.itemView = itemView;
    }

    public static HintOperatorHolder getInstance() {
        LayoutInflater inflater = LayoutInflater.from(App.getCurrentActivity());
        return new HintOperatorHolder(inflater.inflate(R.layout.hint_operator_holder, null, false));
    }

    public void update(HintModel hintModel) {
        ((TextView) itemView.findViewById(R.id.hint_text)).setText(hintModel.getText());
    }
}

package com.example.stus.kvesty.holders;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.stus.kvesty.App;
import com.example.stus.kvesty.R;
import com.example.stus.kvesty.Utils;
import com.example.stus.kvesty.models.recycler_view_models.HintModel;

/**
 * Created by stus on 06.10.17.
 */

public class HintImmageHolder extends RecyclerView.ViewHolder {
    private View itemView;
    private ImageView taskPicture;
    private ImageView fullScreenTaskPicture;

    public HintImmageHolder(View itemView) {
        super(itemView);
        this.itemView = itemView;
        taskPicture = itemView.findViewById(R.id.hint_image);
        fullScreenTaskPicture = itemView.findViewById(R.id.task_picture_full_screen);
    }


    public static HintImmageHolder getInstance() {
        LayoutInflater inflater = LayoutInflater.from(App.getCurrentActivity());
        return new HintImmageHolder(inflater.inflate(R.layout.hint_image_holder, null, false));
    }

    public void update(HintModel hintModel) {
        taskPicture.setBackgroundDrawable(Utils.getDravableFromAssets(hintModel.getText()));
//        taskPicture.setOnClickListener(v -> {
//            fullScreenTaskPicture.setBackgroundDrawable(Utils.getDravableFromAssets(hintModel.getText()));
//            fullScreenTaskPicture.setVisibility(View.VISIBLE);
//            fullScreenTaskPicture.setOnClickListener(view -> fullScreenTaskPicture.setVisibility(View.GONE));
//        });
    }
}

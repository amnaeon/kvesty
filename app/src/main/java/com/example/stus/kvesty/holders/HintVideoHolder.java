package com.example.stus.kvesty.holders;

import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.VideoView;

import com.example.stus.kvesty.App;
import com.example.stus.kvesty.R;
import com.example.stus.kvesty.models.recycler_view_models.HintModel;

/**
 * Created by stus on 11.10.17.
 */

public class HintVideoHolder extends RecyclerView.ViewHolder {
    private VideoView videoView;

    public HintVideoHolder(View itemView) {
        super(itemView);
        videoView = itemView.findViewById(R.id.task_video);
    }

    public static HintVideoHolder getInstance() {
        LayoutInflater inflater = LayoutInflater.from(App.getCurrentActivity());
        return new HintVideoHolder(inflater.inflate(R.layout.hint_video_holder, null, false));
    }

    public void update(HintModel hintModel) {
        String path = "android.resource://" + App.getCurrentActivity().getPackageName() + "/" + R.raw.w;
        videoView.setVideoURI(Uri.parse(path));
        videoView.setBackgroundColor(Color.TRANSPARENT);
        videoView.setZOrderOnTop(true);
        videoView.setBackgroundDrawable(App.getCurrentActivity().getResources().getDrawable(R.drawable.play_ico));
        videoView.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                if (videoView.isPlaying()) {
                    videoView.pause();
                } else {
                    videoView.start();
                }
            }
            return true;
        });
    }
}

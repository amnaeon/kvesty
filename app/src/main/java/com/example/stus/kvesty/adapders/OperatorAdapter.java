package com.example.stus.kvesty.adapders;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.example.stus.kvesty.holders.OperatorHolder;
import com.example.stus.kvesty.interfaces.IOnTeamClick;
import com.example.stus.kvesty.models.mongo_models.MongoTeamModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stus on 11.10.17.
 */

public class OperatorAdapter extends RecyclerView.Adapter<OperatorHolder> {
    private IOnTeamClick onTeamClick;
    private List<MongoTeamModel> data = new ArrayList<>();


    public OperatorAdapter(List<MongoTeamModel> data) {
        this.data = data;
    }

    @Override
    public OperatorHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return OperatorHolder.getInstance();
    }

    @Override
    public void onBindViewHolder(OperatorHolder holder, int position) {
        holder.update(data.get(position));
        holder.setOnTeamClick(onTeamClick);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setOnTeamClick(IOnTeamClick onTeamClick) {
        this.onTeamClick = onTeamClick;
    }
}

package com.example.stus.kvesty.fragments;

import android.os.Bundle;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.Toast;

import com.example.stus.kvesty.App;
import com.example.stus.kvesty.R;
import com.example.stus.kvesty.Socket;
import com.example.stus.kvesty.adapders.OperatorAdapter;
import com.example.stus.kvesty.interfaces.ILink;
import com.example.stus.kvesty.interfaces.IOnAnswerSend;
import com.example.stus.kvesty.interfaces.IOnTeamClick;
import com.example.stus.kvesty.interfaces.ISocketResponce;
import com.example.stus.kvesty.models.mongo_models.MongoGameModel;
import com.example.stus.kvesty.models.mongo_models.MongoTeamModel;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.stus.kvesty.Const.BundleKeys.DIALOG_TEXT;
import static com.example.stus.kvesty.Const.BundleKeys.GAME_ID;
import static com.example.stus.kvesty.Const.SocketCmd.GAME_START;
import static com.example.stus.kvesty.Const.SocketCmd.GOT_A_HINT;

/**
 * Created by stus on 11.10.17.
 */

public class OperatorFragment extends BaseFragment implements IOnTeamClick, ISocketResponce, IOnAnswerSend {
    private String gameId = "";
    private Socket socket;
    private MongoTeamModel model;
    private Dialog fd;
    private Button startGame;
    private Button sendHint;

    @Override
    protected int viewID() {
        return R.layout.operator_fragment;
    }

    public static OperatorFragment getInstance(String gameId) {
        Bundle arg = new Bundle();
        arg.putString(GAME_ID, gameId);
        OperatorFragment instance = new OperatorFragment();
        instance.setArguments(arg);
        return instance;
    }

    @Override
    protected Initer getInit() {
        return super.getInit()
                .add(this::readArgs)
                .add(this::initTopPannel)
                .add(this::initSocket)
                .add(this::initTeamList);
    }

    private void readArgs() {
        gameId = getArguments().getString(GAME_ID);
    }

    private void initTopPannel() {
        startGame = parent.findViewById(R.id.start_game);
        sendHint = parent.findViewById(R.id.send_hint);
        startGame.setOnClickListener(v -> {
            AlertDialog.Builder adb = new AlertDialog.Builder(App.getCurrentActivity());
            adb.setTitle("Вы уверены что хотите начать игру?");
            adb.setPositiveButton("Да", (dialogInterface, i) -> {
                JSONObject object = new JSONObject();
                try {
                    object.put("cmd", GAME_START);
                    socket.sendComand(object.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            });
            adb.setNegativeButton("Нет", (dialogInterface, i) -> dialogInterface.dismiss());
            adb.create();
            adb.show();
        });
    }

    private void initSocket() {
        socket = new Socket();
        socket.setSocketResponce(this);
        socket.createWebSocketClient();

    }

    private void initTeamList() {
        RecyclerView teamList = parent.findViewById(R.id.team_list);
        teamList.setLayoutManager(new LinearLayoutManager(App.getCurrentActivity()));
        ILink iLink = ILink.retrofit.create(ILink.class);
        Call<List<MongoTeamModel>> call = iLink.getAllTeams(gameId);
        call.enqueue(new Callback<List<MongoTeamModel>>() {
            @Override
            public void onResponse(Call<List<MongoTeamModel>> call, Response<List<MongoTeamModel>> response) {
                OperatorAdapter adapter = new OperatorAdapter(response.body());
                adapter.setOnTeamClick(OperatorFragment.this);
                teamList.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<MongoTeamModel>> call, Throwable t) {

            }
        });

    }

    @Override
    public void onClick(MongoTeamModel model) {
        this.model = model;
        fd = new Dialog();
        Bundle arg = new Bundle();
        arg.putString(DIALOG_TEXT, "Введите подсказку команде " + model.getName());
        fd.setArguments(arg);
        fd.setOnAnswerSend(this);
        fd.show(getFragmentManager(), "ds");
    }

    @Override
    public void complete() {
        App.getCurrentActivity().runOnUiThread(() -> initTeamList());
    }

    @Override
    public void gameStart() {
        App.getCurrentActivity().runOnUiThread(() -> Toast.makeText(App.getCurrentActivity(), "Игра начата", Toast.LENGTH_LONG).show());

    }

    @Override
    public void gameEnd(String name) {
        App.getCurrentActivity().runOnUiThread(() -> {
            Toast.makeText(App.getCurrentActivity(), "команда " + name + " победила", Toast.LENGTH_LONG).show();
        });
    }

    @Override
    public void getHint(String teamId) {
        App.getCurrentActivity().runOnUiThread(() -> Toast.makeText(App.getCurrentActivity(), "Подсказка оптравленна", Toast.LENGTH_LONG).show());
    }

    @Override
    public void send(String text) {
        fd.dismiss();
        JSONObject command = new JSONObject();
        try {
            command.put("cmd", GOT_A_HINT);
            command.put("teamId", model.getId());
            command.put("text", text);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        socket.sendComand(command.toString());
    }
}

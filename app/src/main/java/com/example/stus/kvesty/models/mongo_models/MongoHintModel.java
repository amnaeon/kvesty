package com.example.stus.kvesty.models.mongo_models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by stus on 06.10.17.
 */

public class MongoHintModel {
    private String videoUrl = "";
    private String photoUrl = "";
    private String flashUrl = "";
    private String text = "";
    private int time = 0;

    public MongoHintModel(JSONObject jsonObject) throws JSONException {
        videoUrl = jsonObject.getString("videoUrl");
        photoUrl = jsonObject.getString("photoUrl");
        flashUrl = jsonObject.getString("flashUrl");
        text = jsonObject.getString("text");
        time = jsonObject.getInt("time");
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public String getFlashUrl() {
        return flashUrl;
    }

    public String getText() {
        return text;
    }

    public int getTime() {
        return time;
    }
}

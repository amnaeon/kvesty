package com.example.stus.kvesty.adapders;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.example.stus.kvesty.holders.ProgressHolder;
import com.example.stus.kvesty.models.recycler_view_models.ProgressDataModel;

import java.util.ArrayList;
import java.util.List;


public class ProgressAdapter extends RecyclerView.Adapter<ProgressHolder> {
    private List<ProgressDataModel> data = new ArrayList<>();

    public ProgressAdapter(List<ProgressDataModel> data) {
        this.data = data;
    }

    @Override
    public ProgressHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return ProgressHolder.getInstance();
    }

    @Override
    public void onBindViewHolder(ProgressHolder holder, int position) {
        holder.update(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}

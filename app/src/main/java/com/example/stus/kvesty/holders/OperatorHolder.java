package com.example.stus.kvesty.holders;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.stus.kvesty.App;
import com.example.stus.kvesty.R;
import com.example.stus.kvesty.interfaces.IOnTeamClick;
import com.example.stus.kvesty.models.mongo_models.MongoTeamModel;

/**
 * Created by stus on 11.10.17.
 */

public class OperatorHolder extends RecyclerView.ViewHolder {
    private TextView teamName;
    private TextView progress;
    private Button sendHint;
    private IOnTeamClick onTeamClick;

    public OperatorHolder(View itemView) {
        super(itemView);
        teamName = itemView.findViewById(R.id.team_name);
        progress = itemView.findViewById(R.id.team_progress);
        sendHint = itemView.findViewById(R.id.write);
    }

    public static OperatorHolder getInstance() {
        LayoutInflater inflater = LayoutInflater.from(App.getCurrentActivity());
        return new OperatorHolder(inflater.inflate(R.layout.operator_team_list_holder, null, false));
    }

    public void update(MongoTeamModel model) {
        teamName.setText(model.getName());
        progress.setText("выполненно заданий " + model.getTeamProgress());
        sendHint.setOnClickListener(v -> onTeamClick.onClick(model));
    }

    public void setOnTeamClick(IOnTeamClick onTeamClick) {
        this.onTeamClick = onTeamClick;
    }
}

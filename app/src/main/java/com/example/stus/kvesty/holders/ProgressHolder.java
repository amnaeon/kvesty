package com.example.stus.kvesty.holders;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.stus.kvesty.App;
import com.example.stus.kvesty.R;
import com.example.stus.kvesty.models.recycler_view_models.ProgressDataModel;

/**
 * Created by stus on 05.10.17.
 */

public class ProgressHolder extends RecyclerView.ViewHolder {
    private TextView comandName;
    private TextView progress;
    private TextView taskCount;

    public ProgressHolder(View itemView) {
        super(itemView);
        comandName = itemView.findViewById(R.id.name);
        progress = itemView.findViewById(R.id.progress);
        taskCount = itemView.findViewById(R.id.task_count);
    }

    public static ProgressHolder getInstance() {
        LayoutInflater inflater = LayoutInflater.from(App.getCurrentActivity());
        return new ProgressHolder(inflater.inflate(R.layout.progress_holder, null, false));
    }

    public void update(ProgressDataModel model) {
        comandName.setText(model.getName());
        progress.setText(model.getProgress());
        taskCount.setText(model.getTaskCount());
    }

}

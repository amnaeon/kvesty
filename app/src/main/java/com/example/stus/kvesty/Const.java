package com.example.stus.kvesty;

/**
 * Created by stus on 06.10.17.
 */

public class Const {
    public static final int SPLASH_DELLAY = 500;

    public class HintTypes {
        public static final int IMAGE_TYPE = 1;
        public static final int VIDEO_TYPE = 2;
        public static final int FLASH_TYPE = 3;
        public static final int OPERATOR_TYPE = 4;
    }

    public class SocketCmd {
        public static final int TASK_COMPLETE = 10;
        public static final int GOT_A_HINT = 20;
        public static final int TIMER_STOP = 30;
        public static final int TIMER_START = 40;
        public static final int GAME_START = 60;
        public static final int GAME_END = 70;
    }

    public class BundleKeys {
        public static final String GAME_ID = "gameID";
        public static final String DIALOG_TEXT = "dialogText";
        public static final String TEAM_ID = "teamId";
        public static final String DURATION = "duration";
    }

    public class SharedPreference{
        public static final String TEAM_PROGRESS = "team_progress";
        public static final String HINT_PROGRESS = "hint_progress";

    }
}

package com.example.stus.kvesty.models.mongo_models;

/**
 * Created by stus on 05.10.17.
 */

public class MongoUserModel {
    private String id = "";
    private String userId = "";
    private String login = "";
    private String password = "";
    private String teamId = "";

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getTeamId() {
        return teamId;
    }
}

package com.example.stus.kvesty;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static android.content.ContentValues.TAG;

/**
 * Created by stus on 05.10.17.
 */

public class Utils {
    public static String getId() {
        return android.provider.Settings.System.getString(App.getCurrentActivity().getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
    }

    public static void unzip(String zipFile, String location) throws IOException {
        try {
            File f = new File(location);
            if (!f.isDirectory()) {
                f.mkdirs();
            }
            ZipInputStream zin = new ZipInputStream(new FileInputStream(zipFile));
            try {
                ZipEntry ze = null;
                while ((ze = zin.getNextEntry()) != null) {
                    String path = location + ze.getName();

                    if (ze.isDirectory()) {
                        File unzipFile = new File(path);
                        if (!unzipFile.isDirectory()) {
                            unzipFile.mkdirs();
                        }
                    } else {
                        FileOutputStream fout = new FileOutputStream(path, false);
                        try {
                            for (int c = zin.read(); c != -1; c = zin.read()) {
                                fout.write(c);
                            }
                            zin.closeEntry();
                        } finally {
                            fout.close();
                        }
                    }
                }
            } finally {
                zin.close();
            }
        } catch (Exception e) {
            Log.e(TAG, "Unzip exception", e);
        }
    }

    public static String readTextFromAssets(String name) {
        String text = "";
        try {
            InputStream is = App.getCurrentActivity().getAssets().open(name);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            text = new String(buffer);
        } catch (IOException ex) {
            Log.i("kek", "kek");
        }
        return text;
    }

    public static Drawable getDravableFromAssets(String name) {
        Drawable d = null;
        try {
            InputStream ims = App.getCurrentActivity().getAssets().open(name);
            d = Drawable.createFromStream(ims, null);
        } catch (IOException ex) {
        }
        return d;
    }

    public static double taminotoC(String firstStr, String secondStr) {
        double a = firstStr.length();
        double b = secondStr.length();
        double c = 0;
        for (int i = 0; i < firstStr.length(); i++) {
            for (int j = 0; j < secondStr.length(); j++) {
                if (Character.toLowerCase(firstStr.charAt(i)) == Character.toLowerCase(secondStr.charAt(j))) {
                    c++;
                    break;
                }
            }
        }
        return c / (a + b - c);
    }


}

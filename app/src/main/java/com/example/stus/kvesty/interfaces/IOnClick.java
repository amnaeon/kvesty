package com.example.stus.kvesty.interfaces;


import com.example.stus.kvesty.models.mongo_models.MongoGameModel;

public interface IOnClick {
    void onClick(MongoGameModel model);
}
package com.example.stus.kvesty.adapders;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.example.stus.kvesty.holders.HintImmageHolder;
import com.example.stus.kvesty.holders.HintOperatorHolder;
import com.example.stus.kvesty.holders.HintVideoHolder;
import com.example.stus.kvesty.models.recycler_view_models.HintModel;

import java.util.ArrayList;
import java.util.List;

import static com.example.stus.kvesty.Const.HintTypes.*;


/**
 * Created by stus on 06.10.17.
 */

public class HintAdapter extends RecyclerView.Adapter {
    private List<HintModel> data = new ArrayList<>();

    public HintAdapter(List<HintModel> data) {
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case IMAGE_TYPE:
                return HintImmageHolder.getInstance();
            case VIDEO_TYPE:
                return HintVideoHolder.getInstance();
            case FLASH_TYPE:
                return null;
            case OPERATOR_TYPE:
                return HintOperatorHolder.getInstance();

        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = getItemViewType(position);
        switch (type) {
            case IMAGE_TYPE:
                ((HintImmageHolder) holder).update(data.get(position));
                break;
            case VIDEO_TYPE:
                ((HintVideoHolder) holder).update(data.get(position));
                break;
            case FLASH_TYPE:
                break;
            case OPERATOR_TYPE:
                ((HintOperatorHolder) holder).update(data.get(position));
                break;

        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        int type = 0;
        switch (data.get(position).getType()) {
            case IMAGE_TYPE:
                type = IMAGE_TYPE;
                break;
            case VIDEO_TYPE:
                type = VIDEO_TYPE;
                break;
            case FLASH_TYPE:
                type = FLASH_TYPE;
                break;
            case OPERATOR_TYPE:
                type = OPERATOR_TYPE;
                break;

        }
        return type;
    }
}

package com.example.stus.kvesty.interfaces;

/**
 * Created by stus on 10.10.17.
 */

public interface ISocketResponce {
    void complete();
    void gameStart();
    void gameEnd(String name);
    void getHint(String teamId);
}

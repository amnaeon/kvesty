package com.example.stus.kvesty.holders;

import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.stus.kvesty.App;
import com.example.stus.kvesty.interfaces.IOnClick;
import com.example.stus.kvesty.R;
import com.example.stus.kvesty.models.mongo_models.MongoGameModel;

/**
 * Created by stus on 05.10.17.
 */

public class GameHolder extends RecyclerView.ViewHolder {
    private IOnClick onClick;
    private ImageView gameImmage;
    private TextView gameName;
    private View itemView;

    public GameHolder(View itemView) {
        super(itemView);
        this.itemView = itemView;
        gameImmage = itemView.findViewById(R.id.game_immage);
        gameName = itemView.findViewById(R.id.game_name_text);
    }

    public static GameHolder getInstance() {
        LayoutInflater inflater = LayoutInflater.from(App.getCurrentActivity());
        return new GameHolder(inflater.inflate(R.layout.game_item, null, false));
    }

    public void update(MongoGameModel desktopGameModel) {
        byte[] background = Base64.decode(desktopGameModel.getPhotoUrl(),Base64.DEFAULT);
        gameImmage.setBackgroundDrawable(new BitmapDrawable(App.getCurrentActivity().getResources(),BitmapFactory.decodeByteArray(background, 0, background.length)));
        gameName.setText(desktopGameModel.getName());
        itemView.setOnClickListener(v -> onClick.onClick(desktopGameModel));
    }

    public void setOnClick(IOnClick onClick) {
        this.onClick = onClick;
    }
}

package com.example.stus.kvesty.adapders;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.example.stus.kvesty.interfaces.IOnClick;
import com.example.stus.kvesty.holders.GameHolder;
import com.example.stus.kvesty.models.mongo_models.MongoGameModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stus on 05.10.17.
 */

public class GameAdapter extends RecyclerView.Adapter<GameHolder> {

    private List<MongoGameModel> data = new ArrayList<>();
    private IOnClick onClick;

    public GameAdapter(List<MongoGameModel> data) {
        this.data = data;
    }

    @Override
    public GameHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return GameHolder.getInstance();
    }

    @Override
    public void onBindViewHolder(GameHolder holder, int position) {
        holder.setOnClick(onClick);
        holder.update(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setOnClick(IOnClick onClick) {
        this.onClick = onClick;
    }
}
